﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;

namespace Elevator
{
    class ElevatorEmulator
    {
        private readonly int _waitingPassenger = 2;
        private readonly int _waitingResponse = 1;
        private List<int> _evoked = new List<int>();
        private object _locker;

        public ElevatorEmulator(object locker)
        {
            _locker = locker;
        }

        public int NumberFloors { get; set; }
        public double HeightStorey { get; set; }
        public double Speed { get; set; }
        public double OpeningTime { get; set; }

        private int DirectionMovement { get; set; } = 1;
        private int CurentNumberStorey { get; set; } = 1;

        public void AddStorey(int number)
        {
            if (IsValideStorey(number))
            {
                if (!_evoked.Contains(number))
                    _evoked.Add(number);
            }
            else
            {
                WriteLineLog("Invalid number storey!");
            }
        }
        public void Run()
        {
            while (true)
            {
                lock (_locker)
                {
                    if (_evoked.Any())
                    {
                        if (NeedChangeDirection())
                            DirectionMovement *= -1;
                        int next = GetNextStorey();
                        if(next == CurentNumberStorey)
                            OpenDoor();
                        else
                        {
                            GoToTheNextStorey();
                        }
                    }
                    else
                    {
                        StartProcess(null, _waitingResponse, null);
                    }
                }
            }
        }

        private void GoToTheNextStorey()
        {            
            StartProcess(null, HeightStorey / Speed, $"The lift is on the {CurentNumberStorey+DirectionMovement}th floor.");
            CurentNumberStorey += DirectionMovement;
        }
        private void OpenDoor()
        {
            _evoked.Remove(CurentNumberStorey);

            StartProcess(null, OpeningTime, "Elevator opened the doors.");
            StartProcess("Waiting for passengers...", _waitingPassenger, null);
            StartProcess(null, OpeningTime, "Elevator closed the doors.");

            if (!_evoked.Any())
                WriteLog("Evoke to storey: ");
        }
        private int GetNextStorey()
        {
            if (_evoked.Contains(CurentNumberStorey))
                return CurentNumberStorey;
            
            var nearest = (DirectionMovement > 0)
                ? _evoked.Where(storey => storey > CurentNumberStorey).OrderBy(s => s)
                : _evoked.Where(storey => storey < CurentNumberStorey).OrderByDescending(s => s);
            return nearest.FirstOrDefault(); 
        }
        private void StartProcess(string preMessage, double sec, string postMessage)
        {
            if (!String.IsNullOrEmpty(preMessage))
                WriteLineLog(preMessage);

            Thread.Sleep((int)sec * 1000);

            if(!String.IsNullOrEmpty(postMessage))
                WriteLineLog(postMessage);
        }

        private bool NeedChangeDirection() => GetNextStorey() == 0;
        private bool IsValideStorey(int number) => number > 0 && number <= NumberFloors;
        private void WriteLineLog(string log) => Console.WriteLine(log);
        private void WriteLog(string log) => Console.Write(log);
    }
}
