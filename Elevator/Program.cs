﻿using System;
using System.Threading;

namespace Elevator
{
    class Program
    {
        static void Main(string[] args)
        {
            bool init = false;
            int N = 0;
            double heightStorey = 0,
                speed = 0,
                openingTime = 0;

            if (args.Length == 4)
            {
                try
                {
                    N = int.Parse(args[0]);
                    heightStorey = double.Parse(args[1]);
                    speed = double.Parse(args[2]);
                    openingTime = double.Parse(args[3]);

                    init = IsCorrectNumberFloors(N) &&
                        heightStorey > 0 && speed > 0 && openingTime > 0;
                }
                catch (Exception) { }
                if (!init)
                {
                    Console.WriteLine("Input parameters are invalid!");
                }
            }
            else if (args.Length != 0)
            {
                Console.WriteLine("Wrong number of input parameters!");
            }
            if (!init)
            {
                do
                {
                    Console.Write("Number of floors (from 5 to 20): ");
                } while (!int.TryParse(Console.ReadLine(), out N) || !IsCorrectNumberFloors(N));

                do
                {
                    Console.Write("Height of storey: ");
                } while (!double.TryParse(Console.ReadLine(), out heightStorey) || heightStorey <= 0);

                do
                {
                    Console.Write("Elevator speed: ");
                } while (!double.TryParse(Console.ReadLine(), out speed) || speed <= 0);

                do
                {
                    Console.Write("Opening time of elevator doors: ");
                } while (!double.TryParse(Console.ReadLine(), out openingTime) || openingTime <= 0);
            }

            object locker = new object();
            ElevatorEmulator elevator = new ElevatorEmulator(locker);
            elevator.HeightStorey = heightStorey;
            elevator.NumberFloors = N;
            elevator.OpeningTime = openingTime;
            elevator.Speed = speed;

            Thread thread = new Thread(elevator.Run);
            thread.Start();

            int storey;
            Console.Write("Evoke to storey: ");
            while (true)
            {                
                if (int.TryParse(Console.ReadLine(), out storey))
                {
                    lock (locker)
                    {
                        elevator.AddStorey(storey);
                    }                    
                }                    
            }
        }

        static bool IsCorrectNumberFloors(int N) => N >= 5 && N <= 20;
    }
}
